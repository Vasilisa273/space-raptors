// Подключение библиотек
#include <SPI.h>
#include <MFRC522.h>
#include <Servo.h>

Servo servo1;

// константы подключения контактов SS и RST
#define RST_PIN 9
#define SS_PIN 10

// Инициализация MFRC522
MFRC522 mfrc522(SS_PIN, RST_PIN);

bool isFirstRead = false;

void setup()
{
  servo1.attach(8);
  Serial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();
}

void loop()
{
  if (!mfrc522.PICC_IsNewCardPresent())
    return;

  if (!mfrc522.PICC_ReadCardSerial())
    return;

  Serial.print(F("Card UID:"));
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  Serial.println();

  Serial.print(F("PICC type: "));
  byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));
  delay(2000);

  if (isFirstRead)
  {
    servo1.write(180);
    isFirstRead = false;
  }
  else
  {
    servo1.write(0);
    isFirstRead = true;
  }

  delay(2000);
}

void dump_byte_array(byte *buffer, byte bufferSize)
{
  for (byte i = 0; i < bufferSize; i++)
  {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}